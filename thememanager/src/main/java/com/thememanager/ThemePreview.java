package com.thememanager;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dingfeng on 2016/7/21.
 */
public class ThemePreview extends AppCompatActivity {


    private TextView txtName;
    private ViewPager mViewPager;
    private Button btnStart;

    private PreviewAdapter mPreviewAdapter;
    private List<View> mViewList = new ArrayList<>();
    private Drawable[] mPreviewIcon = new Drawable[3];
    private String mThemePkg;
    private String mThemeName;

    public static final String BROADCAST_UPDATE_THEME = "com.theme.update";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme_preview);

        txtName = (TextView) findViewById(R.id.txtName);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        btnStart = (Button) findViewById(R.id.btnStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(BROADCAST_UPDATE_THEME);
                intent.putExtra("theme_package", mThemePkg);
                sendBroadcast(intent);
            }
        });

        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        if (intent != null) {
            mThemePkg = intent.getStringExtra("theme_package");
            try {
                PackageManager pm = getPackageManager();
                PackageInfo packageInfo = pm.getPackageInfo(mThemePkg, 0);
                if (packageInfo != null) {
                    ApplicationInfo appInfo = packageInfo.applicationInfo;
                    mThemeName = appInfo.loadLabel(pm).toString();

                    Context themeContext = createPackageContext(mThemePkg, CONTEXT_IGNORE_SECURITY);
                    Resources themeResources = themeContext.getResources();

                    mPreviewIcon[0] = ReflectionUtil.drawable(themeResources, mThemePkg, "preview_1");
                    mPreviewIcon[1] = ReflectionUtil.drawable(themeResources, mThemePkg, "preview_2");
                    mPreviewIcon[2] = ReflectionUtil.drawable(themeResources, mThemePkg, "preview_3");

                    for (int i = 0; i < mPreviewIcon.length; i++) {
                        ImageView imageView = new ImageView(this);
                        imageView.setImageDrawable(mPreviewIcon[i]);
                        mViewList.add(imageView);
                    }
                    mPreviewAdapter = new PreviewAdapter(mViewList);
                    mViewPager.setAdapter(mPreviewAdapter);
                    txtName.setText(mThemeName);
                }

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

        }
    }

    class PreviewAdapter extends PagerAdapter {

        private List<View> mListViews;

        public PreviewAdapter(List<View> mListViews) {
            this.mListViews = mListViews;
        }

        @Override
        public int getCount() {
            return mListViews.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(mListViews.get(position), 0);
            return mListViews.get(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(mListViews.get(position));
        }
    }


}
